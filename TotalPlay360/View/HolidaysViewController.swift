//
//  HolidaysViewController.swift
//  TotalPlay360
//
//  Created by fer on 09/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class HolidaysViewController: UIViewController, UIAlertViewDelegate {

    @IBOutlet weak var imageUser1: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        imageUser1.layer.cornerRadius = imageUser1.frame.size.width / 2
        imageUser1.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func select_alert_action(_ sender: UIButton) {
        // show alert
        
        let vc = self.storyboard?.instantiateViewController(withIdentifier: "CustomAlertViewController") as! CustomAlertViewController
        vc.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        self.addChildViewController(vc)
        self.view.addSubview(vc.view)
        
    }
    
        // Initialize Alert View
        //let alertView = UIAlertView(title: "Confirmación", message: "El periodo que solicitaste es del 02-Mayo-2018 al 10-Mayo-2018. ¿Deseas confirmar?", delegate: self, cancelButtonTitle: "Confirmar", otherButtonTitles: "Modificar")
        
        // Configure Alert View
        //alertView.tag = 1
        
        // Show Alert View
        //alertView.show()
       
  
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
