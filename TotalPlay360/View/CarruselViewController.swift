//
//  CarruselViewController.swift
//  TotalPlay360
//
//  Created by fer on 29/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit
import SwiftBaseLibrary

class CarruselViewController: BaseViewController, iCarouselDataSource, iCarouselDelegate {

    @IBOutlet weak var mCarrusel: iCarousel!
    
    
    var arrayItems = [Image]()
    
    @IBOutlet weak var Img1: UIImageView!
    @IBOutlet weak var Img2: UIImageView!
    @IBOutlet weak var Img3: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let myColor = UIColor(red:108/255.0, green:36/255.0, blue:251/255.0, alpha: 1.0)
        
        Img1.layer.cornerRadius = Img1.frame.size.width / 2
        Img1.clipsToBounds = true

        Img2.layer.cornerRadius = Img2.frame.size.width / 2
        Img2.clipsToBounds = true
        Img2.layer.borderWidth = 2
        Img2.layer.borderColor = myColor.cgColor
        
        Img3.layer.cornerRadius = Img3.frame.size.width / 2
        Img3.clipsToBounds = true
        Img3.layer.borderWidth = 2
        Img3.layer.borderColor = myColor.cgColor
        
        
        let obj: Image = Image()
        obj.image = "img_girl"
        obj.tittle = "Diseño"
        arrayItems.append(obj)
        
        
        let obj1: Image = Image()
        obj1.image = "img_girl"
        obj1.tittle = "Requerimientos \nTotalplay"
        arrayItems.append(obj1)
        
        
        let obj2: Image = Image()
        obj2.image = "img_boy"
        obj2.tittle = "Requerimientos \nTotalplay \nEmpresarial"
        arrayItems.append(obj2)
        
        
        let obj3: Image = Image()
        obj3.image = "img_girl"
        obj3.tittle = "Diseño"
        arrayItems.append(obj3)
        
        
        
        mCarrusel.type = .linear
        mCarrusel.delegate = self
        mCarrusel.dataSource = self
        mCarrusel.reloadData()
        // Do any additional setup after loading the view.
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: icarousel delegate methods
    
    func numberOfItems(in carousel: iCarousel) -> Int
    {
        return arrayItems.count
    }
    
    func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView
    {
        let view = Bundle.main.loadNibNamed("ItemCellCollectionReusableView", owner: self, options: nil)![0] as! ItemCellCollectionReusableView
        /*view.frame.size = CGSize(width: self.view.frame.size.width/2, height: 83.0)
         view.backgroundColor = UIColor.lightGray*/
        
        view.mImageCellView.image = UIImage(named:arrayItems[index].image!)
        view.tittleCellView.text = arrayItems[index].tittle!
        return view
    }
    
   
    
    override func viewWillAppear(_ animated: Bool) {
        self.setNeedsStatusBarAppearanceUpdate()
        self.navigationController?.isNavigationBarHidden = true
        
    }
    
    
    func carouselCurrentItemIndexDidChange(_ carousel: iCarousel) {
        
    }
    
}


