//
//  LoginViewController.swift
//  TotalPlay360
//
//  Created by fer on 30/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController , UIAlertViewDelegate{

    
    
    
    var opcion = 1
    @IBOutlet weak var NumeroEmpTextField: UITextField!
    @IBOutlet weak var NumeroEmpView: UIView!
    @IBOutlet weak var NumeroEmpLabel: UILabel!
    @IBOutlet weak var NombreUsuarioLabel: UILabel!
    @IBOutlet weak var UsuarioImage: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

            NombreUsuarioLabel.isHidden = true
            UsuarioImage.isHidden = true
            UsuarioImage.layer.cornerRadius = UsuarioImage.frame.size.width / 2
            UsuarioImage.clipsToBounds = true
        

        
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
       
        
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func prueba(_ sender: Any) {
        if opcion == 2 {
            let alertView = UIAlertController(title: "", message: "Desea activar el Touch ID para iniciar sesión", preferredStyle: .alert)
            let yesAction = UIAlertAction(title: "Si", style: .default) {
                (action) -> Void in
                self.opcion = 3
                
            }
            let noAction = UIAlertAction(title: "No", style: .default) {
                (action) -> Void in
                self.opcion = 2
            }
            
            alertView.addAction(yesAction)
            alertView.addAction(noAction)
            
            self.present(alertView, animated: true, completion: nil)
        
        }
        else if opcion == 1{
            opcion = 2
            NumeroEmpTextField.isHidden = true
            NumeroEmpView.isHidden = true
            NumeroEmpLabel.isHidden = true
            NombreUsuarioLabel.isHidden = false
            UsuarioImage.isHidden = false
        }

        
        
    }
    
 
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
