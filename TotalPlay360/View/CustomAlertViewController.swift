//
//  CustomAlertViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 19/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class CustomAlertViewController: UIViewController {

    @IBAction func modify_action(_ sender: UIButton) {
        //modify alert
        self.removeFromParentViewController()
        self.view.removeFromSuperview()
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
