//
//  ComunicacionViewController.swift
//  TotalPlay360
//
//  Created by fer on 06/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ComunicacionViewController: UIViewController {

    @IBOutlet weak var avanceProgress1:
    UIProgressView!
    @IBOutlet weak var avanceProgress2:
    UIProgressView!
    @IBOutlet weak var avanceProgress3:
    UIProgressView!
    @IBOutlet weak var avanceProgress4:
    UIProgressView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let timer = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceProgress1.setProgress(self.avanceProgress1.progress + 10, animated: true)
            
            if self.avanceProgress1.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        let timer2 = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceProgress2.setProgress(self.avanceProgress2.progress + 10, animated: true)
            
            if self.avanceProgress2.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        
        let timer3 = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceProgress3.setProgress(self.avanceProgress3.progress + 10, animated: true)
            
            if self.avanceProgress3.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
        let timer4 = Timer.scheduledTimer(withTimeInterval: 2, repeats: true) {
            (timer:Timer) in
            
            self.avanceProgress4.setProgress(self.avanceProgress4.progress + 10, animated: true)
            
            if self.avanceProgress4.progress >= 1 {
                timer.invalidate()
            }
            
            timer.fire()
        }
        
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}
