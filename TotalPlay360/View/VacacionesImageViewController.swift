//
//  VacacionesImageViewController.swift
//  TotalPlay360
//
//  Created by Claudia Isamar Delgado Vasquez on 15/02/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class VacacionesImageViewController: UIViewController {

    @IBOutlet weak var ImageUser: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ImageUser.layer.cornerRadius = ImageUser.frame.size.width / 2
        ImageUser.clipsToBounds = true
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
