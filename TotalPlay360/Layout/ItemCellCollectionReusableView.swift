//
//  ItemCellCollectionReusableView.swift
//  TotalPlay360
//
//  Created by fer on 22/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//

import UIKit

class ItemCellCollectionReusableView: UICollectionReusableView {
  
   
    @IBOutlet weak var tittleCellView: UILabel!
    
    @IBOutlet weak var mImageCellView: UIImageView!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        mImageCellView.layer.cornerRadius = mImageCellView.frame.size.width / 2
        mImageCellView.clipsToBounds = true
    }
    
}
