//
//  Image.swift
//  TotalPlay360
//
//  Created by fer on 23/01/18.
//  Copyright © 2018 TotalPlay. All rights reserved.
//


import Foundation
import ObjectMapper

class Image: NSObject, Mappable {
    
    var image : String?
    var tittle : String?
    
    
    public required convenience init?(map: Map) {
        self.init()
    }
    
    public func mapping(map: Map) {
        
        image        <- map[""]
        tittle       <- map[""]
    }
    
        
    }

